package Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.stayfit.R
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
abstract class Frag2 : Fragment(),View.OnClickListener {

    var BtnFasting: Button? = null
    var BtnCarb: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root: View = inflater.inflate(R.layout.fragment_frag2, container, false)
        BtnFasting = root.findViewById(R.id.idBtnFragFasting)
        BtnCarb = root.findViewById(R.id.idBtnFragCarb)

        BtnFasting!!.setOnClickListener(this)
        BtnCarb!!.setOnClickListener(this)

        return root

    }

    override fun onClick(p0: View?) {


        if (p0!!.id == BtnFasting!!.id) {
            Snackbar.make(p0, R.string.snackbar_msg1, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()

        } else {
            if (p0.id == BtnCarb!!.id) {
                Snackbar.make(p0, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }


        }
    }
}





    /*
    fun onClickFasting(v:View){
        Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()

    }

    fun onClickCarb(v:View){
        Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()

    }

}*/