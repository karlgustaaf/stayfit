package com.example.stayfit

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    val TAG: String = "MainActivity"
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth = FirebaseAuth.getInstance()

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        val bundle= Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, 123456.toString())
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "USER/PASSWORD")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN,bundle)



       /* auth.signOut()

       if (auth.currentUser!=null){
        val intent = Intent(this, MainScrollingActivity::class.java)
        startActivity(intent)
        finish()

        if (auth.currentUser!=null){
        val intent = Intent(this, MainScrollingActivity2::class.java)
        startActivity(intent)
        finish()
        } */

    }

    fun onClickGain(v: View) {
        val emailText: String = idEmail.text.toString()
        val passwordText: String = idPassword.text.toString()

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD, "Karl@123.com")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN,bundle)


        auth.signInWithEmailAndPassword(emailText, passwordText)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")

                    val intent = Intent(this, MainScrollingActivity::class.java)
                    startActivity(intent)
                    finish()

                    /* val user = auth.currentUser
                    Toast.makeText(
                        baseContext, "Successful!!!",
                        Toast.LENGTH_SHORT
                    ).show() */


                } else {
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext,
                        "Mismatch! Check E-mail or Password.",
                        Toast.LENGTH_SHORT
                    ).show()

                }


            }


    }
    fun onClickLose(v: View) {
        val emailText: String = idEmail.text.toString()
        val passwordText: String = idPassword.text.toString()

        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.METHOD, "Karl@123.com")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN,bundle)

        auth.signInWithEmailAndPassword(emailText, passwordText)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")
                    val user = auth.currentUser

                    val intent = Intent(this, MainScrollingActivity2::class.java)
                    startActivity(intent)
                    finish()

                    /* Toast.makeText(
                        baseContext, "Successful!!!",
                        Toast.LENGTH_SHORT
                    ).show() */

                } else {
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext,
                        "Mismatch! Check E-mail or Password.",
                        Toast.LENGTH_SHORT
                    ).show()

                    // ...
                }


            }

    }

    fun onClickSignup(v: View) {
        clLogin.visibility = View.GONE
        clSignup.visibility = View.VISIBLE
        if (idSignupPassword.equals(idSignupPassword2)) {

            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.METHOD, "Karl@123.com")
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP,bundle)

        }
    }

    fun onClickSignupConfirm(v: View) {
        val emailSignupText: String = idSignupEmail.text.toString()
        val passwordSignupText: String = idSignupPassword.text.toString()
        val password2SignupText: String = idSignupPassword2.text.toString()

        if (passwordSignupText.equals(password2SignupText)) {
            auth.createUserWithEmailAndPassword(emailSignupText, passwordSignupText)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "createUserWithEmail:success")
                        val user = auth.currentUser
                        Toast.makeText(
                            baseContext, "User Created!",
                            Toast.LENGTH_SHORT
                        ).show()

                    } else {
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Password Mismatch. Min. 6 chara.",
                            Toast.LENGTH_SHORT
                        ).show()

                    }

                }


        } else {
            Toast.makeText(
                baseContext, "Password Mismatch. Min. 6 chara.",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    fun onClickSignupCancel(v: View) {
        clLogin.visibility = View.VISIBLE
        clSignup.visibility = View.GONE

    }
}


