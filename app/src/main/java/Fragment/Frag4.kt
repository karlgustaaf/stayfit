package Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.stayfit.R
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
abstract class Frag4 : Fragment(),View.OnClickListener {

    var BtnKeto: Button? = null
    var BtnPaleo: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root: View = inflater.inflate(R.layout.fragment_frag3, container, false)
        BtnKeto = root.findViewById(R.id.BtnKeto)
        BtnPaleo = root.findViewById(R.id.BtnPaleo)

        BtnKeto!!.setOnClickListener(this)
        BtnPaleo!!.setOnClickListener(this)

        return root

    }

    override fun onClick(p0: View?) {


        if (p0!!.id == BtnKeto!!.id) {
            Snackbar.make(p0, R.string.snackbar_msg1, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()

        } else {
            if (p0.id == BtnPaleo!!.id) {
                Snackbar.make(p0, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }


        }
    }
}